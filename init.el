
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(require 'org)
(org-babel-load-file
 (expand-file-name "config.org"
                   user-emacs-directory))
;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(column-number-mode t)
;;  '(package-selected-packages
;;    (quote
;;     (expand-region iedit jedi projectile smartparens org-journal helm ido-grid-mode ps-ccrypt auto-complete ein markdown-mode cmake-mode cuda-mode google-c-style auctex matlab-mode go-mode org-bullets polymode moe-theme color-theme-approximate rainbow-delimiters sane-term use-package)))
;;     (magit with-editor use-package smartparens sane-term rainbow-delimiters ps-ccrypt projectile polymode org-journal org-bullets moe-theme matlab-mode markdown-mode magit-popup let-alist jedi ido-grid-mode helm google-c-style go-mode ein cuda-mode color-theme-approximate cmake-mode auctex)))
;;  '(rainbow-delimiters-depth-1-face ((t (:foreground "gray80"))))
;;  '(rainbow-delimiters-depth-2-face ((t (:foreground "forest green"))))
;;  '(rainbow-delimiters-depth-3-face ((t (:foreground "yellow"))))
;;  '(rainbow-delimiters-depth-4-face ((t (:foreground "deep sky blue"))))
;;  '(rainbow-delimiters-depth-5-face ((t (:foreground "orange"))))
;;  '(rainbow-delimiters-depth-6-face ((t (:foreground "lime green"))))
;;  '(tool-bar-mode nil))
