[//]: # (markdown notes)
[//]: # (C-c C-c m: run markdown and show raw html in other buffer)
[//]: # (C-c C-h: list of C-c commands)
[//]: # (C-c C-c l: live preview: requires tidy, looks ok)
[//]: # (C-c C-c p: preview: opens chrome tab)

# Caleb's Emacs Configs

My config files are broken up by function.
The first init file is `~/.emacs.d/init.el`. This calls the following files in `emacsinit/`

- `common.el`: package installation, file changes, sane scrolling, backup directories, etc.
- `modes.el`: programming language specific settings
- `display`: parantheses, highlighting, coloring themes, linum, scrollbar, splashscreen
- `global-kbds`: all my personal keybindings


# Package management
Package management is done using package.el (installed with emacs) and [use-package.el](https://github.com/jwiegley/use-package) (installed at the beginning of common.el if it doesn't exist). This package is fantastic and I highly suggest you use it.

`package.el` is the Emacs package manager, and I added the gnu, marmalade, and melpa repositories.

With use-package it is convenient to initialize and auto-install these packages.
With the `:ensure t` option, it will auto install these packages when emacs starts (if they don't exist). It places these in `~/.emacs.d/elpa/`.
Instead of using the  `:ensure t` option, I ensure all packages by running `(setq use-package-always-ensure t)`.


# Folders in .emacs.d

The following folders are in `/.emacs.d/`:

- `backups` (not under version control): autosaves are here, so they don't clog up the original dir, also a bunch of tmp files
- `bin` (under version control): several scripts I commonly use (linting/compliling)
- `elpa` (not under version control): all packages installed by package.el/use-package
- `initfiles` (under version control): all my init files
<!-- - `flymake_tmp`(under version control):: flymake temporary files are here, so they don't clog up the original dir, and also markdown tmp files -->

# Emacs Server

I have also configured emacs to allow for new emacsclient frames.
This is tricky because when using emacs server, the all the configs are shared, so it's tough to have specific terminal/X versions.
Therefore, running `emacs` or `emacs -nw` will give the best results, and using `emacsclient` will have the best mix of the two.
I have it organized in the four scenarios:

### Lone `emacs` window in X

- runs `lonewindow.el`
- turns on line numbering
- moves to left monitor and fullscreens

### Lone `emacs -nw` window in terminal

- runs `lonewindow.el`
- turns off click-and-drag (causes problems), turns on xterm-mouse stuff

### New `emacsclient -c` frame in X

- moves new frame to left monitor and fullscreens

### New `emacsclient -nw` frame in terminal

- runs `tframe.el`: turns off click-and-drag and turns on xterm-mouse permenantly
