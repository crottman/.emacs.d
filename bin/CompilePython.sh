#!/bin/bash
echo "## pyflakes ##"
pyflakes $1
echo "## pep8 ##"
pep8 $1 --ignore E226,E228,E402  --max-line-length=94

# E402 module level import not at top of file
# E226 missing optional whitespace around operator
# E226 missing optional whitespace around modulo operator

echo "## pylint ##"             # pylint is not redundant!
# pylint --include-ids yes -d C0103,C0111,W0401,W0614,R0914,R0915 $1
# pylint_parse.py C0103,R0913,R0914,R0915,R0912,E1101 $1
pylint -r n --msg-template='{path}:{line}:{column}: {msg_id} {obj} {msg}' \
    -d C0103,R0912,R0913,R0914,R0915,E1101,W0640,I0011,R0204 $1

# C0103 Invalid Name
# R0912 Too many branches (*/12)
# R0913 Too many arguments (*/5)
# R0914 Too many local variables (*/15)
# R0915 Too many statements (*/50)
# E1101 Instance of '_a' has not '_b' member (false positives)
# W0640 Cell variable t defined in loop (false positives)
# I0011 Locally disabling %s

# C0111 Missing docstring - no longer ignoring
# W0401 Wildcard import - no longer ignoringb
# W0614 Unused import from wildcard import - no longer ignoring
# R0204 Redefinition of _ type from _ to _


# for line length, add the following to ~/.pylintrc
# [FORMAT]
# max-line-length=94

# flymake notes:

# green: info (convention) pep8, pylint convention
# red underline: error
# pink underline: pylint warning

# C = convention
# R = refactor
# W = warning
# E = Error


# Non-redundancies:
# pyflakes:
# pep8:
#    too many blank lines, all convention stuff
# pylint:
#    Missing doctrings, unused arguments, import order
