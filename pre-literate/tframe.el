;; This might be annoying for some X applications, therefore
;; this should only be called in a lone emacs -nw session

;; ;; changes scrolling??
;; (global-set-key [mouse-4] (lambda ()
;;                             (interactive)
;;                             (scroll-down 1)))
;; (global-set-key [mouse-5] (lambda ()
;;                             (interactive)
;;                             (scroll-up 1)))

(require 'mouse)                      ;mouse not a downloadable package
(xterm-mouse-mode t)

;; (defun track-mouse (e))
(setq mouse-sel-mode nil)
(dolist (k '([mode-line down-mouse-1] [vertical-line down-mouse-1] [down-mouse-1]))
  (global-unset-key k))

(set-face-background 'default "unspecified-bg")
