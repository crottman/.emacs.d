;; Run this code when client isn't being run, but a lone emacs is.
;; in this case, it's easy to tell if we have an x-frame or not

(when (display-graphic-p)       ;emacs
  (message "opened with: emacs")

  (set-frame-position nil 0 0)
  (make-frame-maximized nil)

  (global-linum-mode)

  )
(unless (display-graphic-p)     ;emacs -nw
  (message "opened with: emacs -nw")
  (load "~/.emacs.d/initfiles/tframe.el")
  )
