;; Determining current system name
(defun system-is-topaz ()
  (interactive)
  (string-equal system-name "topaz.sci.utah.edu")
  )
(defun system-is-polyp ()
  (interactive)
  (string-equal system-name "polyp")
  )
(defun system-is-envy ()
  (interactive)
  (string-equal system-name "envy")
  )

;; PACKAGE MANAGEMENT

;; package.el
;; view all packages: M-x list-packages, M-x package-list-packages
;; (setq package-enable-at-startup nil)    ;this doesn't really save time
(require 'package)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ;; ("melpa" . "https://melpa.milkbox.net/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ;; ("go" . "https://github.com/dominikh/go-mode.el")
                         ))
;; (package-initialize)                    ;slow line, not sure if i can change it
(package-initialize t)
(make-directory "~/.emacs.d/elpa" t)
(let ((default-directory "~/.emacs.d/elpa"))
  (normal-top-level-add-subdirs-to-load-path))

;; download use-package if it doesn't exist
(if (not (package-installed-p 'use-package))
    (progn
      (package-refresh-contents)
      (package-install 'use-package)))

;; Necessary? need it on agate
(require 'use-package)                  ;i think this isn't necessary, at least not here
;; use-package: auto package loading/installing
(eval-when-compile
  (require 'use-package))
(setq use-package-always-ensure t)      ;sets this globally
;; note: upgrade use-package by running "package-refresh-contents" and then "package-list-packages"


;; Reload emacs config
(defun reload-config()
  (interactive)
  (load-file "~/.emacs.d/init.el"))

;; FILE CHANGES
;; Trailing Whitespace
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(defun my-delete-trailing-blank-lines ()
  "Deletes all blank lines at the end of the file."
  (interactive)
  (save-excursion
    (save-restriction
      (widen)
      (goto-char (point-max))
      (delete-blank-lines))))
(add-hook 'before-save-hook 'my-delete-trailing-blank-lines)
;; When saving files, set execute permission if #! is in first line.
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)
;; Tabs
(setq-default indent-tabs-mode nil) ; TAB key creates spaces (somehow still ok w/ Makefile)
(setq-default tab-width 2) ; set tab width to 2 for all buffers
(setq tab-width 2);
;; Always end a file with a newline
(setq require-final-newline t)

;; Auto Saving
;; Save all backup file in these directories. Make if necessary
(make-directory "~/.emacs.d/backups/file-backups" t)
(make-directory "~/.emacs.d/backups/SDB/" t)
(make-directory "~/.emacs.d/backups/autosaves" t)
(make-directory "~/.emacs.d/backups/tmp/" t)
(make-directory "~/.emacs.d/backups/cedet/" t)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/backups/autosaves/\\1" t))
      auto-save-list-file-prefix "~/.emacs.d/backups/autosaves/.saves-"
      backup-directory-alist '((".*" . "~/.emacs.d/backups/file-backups"))
      semanticdb-default-save-directory "~/.emacs.d/backups/SDB/"
      temporary-file-directory "~/.emacs.d/backups/tmp/"
      ac-comphist-file "~/.emacs.d/backups/ac-comphist.dat"
      srecode-map-save-file "~/.emacs.d/backups/cedet/srecode-map.el" ;not sure if this works
      )

;; follow VC-links: i think this won't create a backup
(setq vc-follow-symlinks t)

;; Enable versioning with default values (keep five last versions, I think)
;; i also think that this won't make backups for vc files?
(setq make-backup-files t
      backup-by-copying t
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)


;; Scrolling
;; (require 'smooth-scrolling)
;; NOTE: Changes to smooth-scrolling.el:
;;    change defcustom smooth-scroll-margin 1
;; stockoverflow suggestions:
(setq scroll-margin 1
      scroll-step 1
      scroll-conservatively 10000
      scroll-preserve-screen-position 1
      next-line-add-newlines nil)


;; IDO mode
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)
(setq ido-save-directory-list-file "~/.emacs.d/backups/ido.last")
;; IDO grid
(use-package ido-grid-mode
  :config
  (ido-grid-mode t))

(use-package helm
  :defer t)

;; Annoying yes/no questions
(fset 'yes-or-no-p 'y-or-n-p)

;; autocomplete
(use-package auto-complete
  :defer 2                              ;not sure why necessary?
  :config
  (setq-default ac-sources
                '(ac-source-filename
                  ac-source-abbrev
                  ac-source-dictionary
                  ac-source-words-in-same-mode-buffers))
  (require 'auto-complete-config)   ;not a package
  (ac-config-default)                   ;this breaks for some reason?
  )

;; ORG MODE
(use-package org-bullets)
(use-package org
  :init
  (setq org-src-fontify-natively t)
  (setq org-hide-leading-stars t)
  (add-hook 'org-mode-hook
            (lambda () (org-bullets-mode t)))
  (setq org-todo-keywords
        '((sequence "TODO" "WAIT" "|" "DONE")))
  (setq org-todo-keyword-faces
           '( ("WAIT" . (:foreground "orange2" :weight bold :background "khaki1" :box (:line-width 1 :style none) ))))
  (setq org-ellipsis "⤵")
  :config
  (setq org-log-done t)
  (setq org-agenda-files (list "~/Dropbox/todo.org"))

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((python . t)
     (sh . t)
     (emacs-lisp . t)))
  :bind
  ("C-c l" . org-store-link)
  ("C-c a" . org-agenda)
  ;; ("M-RET" . org-insert-todo-heading)   ;not sure why this never works

  )

;; (define-key global-map "\C-cl" 'org-store-link)
;; (define-key global-map "\C-ca" 'org-agenda)
;; (define-key global-map "\M-\r" 'org-insert-todo-heading)
;; (setq org-agenda-files (list "~/Dropbox/todo.org"))

;; OTHER

;; ccrypt
(use-package ps-ccrypt)

;; better window frame names
(setq frame-title-format (concat  "%b - emacs@" system-name))

;; better unique naming buffers
(require 'uniquify)                     ;uniquify not a package

(setq uniquify-buffer-name-style 'post-forward)

;; (global-set-key (kbd "C-M-/") 'my-expand-file-name-at-point)
;; (defun my-expand-file-name-at-point ()
;;   "Use hippie-expand to expand the filename"
;;   (interactive)
;;   (let ((hippie-expand-try-functions-list '(try-complete-file-name-partially try-complete-file-name)))
;;     (call-interactively 'hippie-expand)))

;; MAGIT: need git >= 1.9.4, need emacs >= 24.4

;; this is slow, and most my files are vc files...so disable for now
(if (version< emacs-version "24.4") ()
  (when (daemonp)                       ;start w/ daemon so it works for commits
    (message "breakpoint1")
    (use-package magit
      :ensure                           ;not sure why i need this...
      :init
      (setq magit-push-arguments nil)
      (setq magit-commit-arguments nil))))
(message "breakpoint2")

;; make '_' to be considered as part of a word (helpful for 'M-s w' = word search,
;; among other things
(modify-syntax-entry ?_ "w")

;; Other shell stuff
(use-package sane-term)

;; Run a shell command function
(defun copy-line-to-term ()
  (interactive)
  ;; step 1: copy the current line
  (let ((beg (line-beginning-position))
        (end (line-end-position)))
    (when mark-active
      (if (> (point) (mark))
          (setq beg (save-excursion (goto-char (mark)) (line-beginning-position)))
        (setq end (save-excursion (goto-char (mark)) (line-end-position)))))
    (kill-ring-save beg end))
  ;; step 2: open ansi-term buffer in new window
  (if (get-buffer "*ansi-term*")
      (progn (message "*A-T* buffer exists")
             (switch-to-buffer-other-window "*ansi-term*"))
    (progn (message "creating *A-T* buffer")
           (switch-to-buffer-other-window "*ATtmp*") ;this is shoddy workmanship
           (ansi-term "/usr/bin/zsh" "ansi-term")
           (kill-buffer "*ATtmp*")
           )
    )
  ;; step 3: paste the copied line
  (term-line-mode)
  (yank)                                ;way to do this w/o kill ring?
  (term-char-mode)
)
;; see if this works: byte compile everything

;; (byte-recompile-directory (expand-file-name "~/.emacs.d") 0) ;this one is slow
(byte-recompile-directory (expand-file-name "~/.emacs.d/initfiles") 0)


;; ;; these don't seem to do anything
;; (use-package auto-compile)
;; (auto-compile-on-load-mode)
;; (auto-compile-on-save-mode)

;; careful, this doesn't give warnings of the file doesn't exist
;; these doesn't seem to work... :(
;; (byte-recompile-file "~/.emacs.d/initfiles/common.el" 0)
;; (byte-recompile-file "~/.emacs.d/initfiles/display.el" 0)
;; (byte-recompile-file "~/.emacs.d/initfiles/envy.el" 0)
;; (byte-recompile-file "~/.emacs.d/initfiles/global-kbds.el" 0)
;; (byte-recompile-file "~/.emacs.d/initfiles/jedi-starter.el" 0)
;; (byte-recompile-file "~/.emacs.d/initfiles/modes.el" 0)

;; these will compile and write (will give warnings)
;; (byte-compile-file "~/.emacs.d/initfiles/common.el")
;; (byte-compile-file "~/.emacs.d/initfiles/display.el") ;this will compile it
;; (byte-compile-file "~/.emacs.d/initfiles/envy.el")
;; (byte-compile-file "~/.emacs.d/initfiles/themes/db-x-theme.el")
;; (byte-compile-file "~/.emacs.d/initfiles/global-kbds.el")
;; (byte-compile-file "~/.emacs.d/initfiles/jedi-starter.el")
;; (byte-compile-file "~/.emacs.d/initfiles/modes.el")

;; ;; Copy full line to
;; (defun copy-line (arg)
;;   "Copy lines (as many as prefix argument) in the kill ring.
;;    Ease of use features:
;;    - Move to start of next line.
;;    - Appends the copy on sequential calls.
;;    \\ - Use newline as last char even on the last line of the buffer.
;;    - If region is active, copy its lines."
;;   (interactive "p")
;;   (let ((beg (line-beginning-position))
;;         (end (line-end-position arg))
;;         (now (point)))
;;     (when mark-active
;;       (if (> (point) (mark))
;;           (setq beg (save-excursion (goto-char (mark)) (line-beginning-position)))
;;         (setq end (save-excursion (goto-char (mark)) (line-end-position)))))
;;     (if (eq last-command 'copy-line)
;;         (kill-append (buffer-substring beg end) (< end beg))
;;       (kill-ring-save beg end)))
;;   ;; (kill-append "\n" nil)
;;   ;; (beginning-of-line (or (and arg (1+ arg)) 2))
;;   (if (and arg (not (= 1 arg))) (message "%d lines copied" arg))
;;   (shell-command-on-region (line-beginning-position) (line-end-position) "xclip -selection clipboard")

;;   )

;; JOURNAL :::::::::::::
(use-package org-journal)
(setq org-journal-dir "~/Dropbox/orgnotes/journal/")
(setq org-journal-file-format "%Y-%m-%d.org")
