;; not necessary for new theme??
(use-package rainbow-delimiters
  :config
  (add-hook 'scheme-mode-hook     'rainbow-delimiters-mode)
  ;; (add-hook 'LaTeX-mode-hook      'rainbow-delimiters-mode)   ;breaks for some reason
  (add-hook 'emacs-lisp-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'ess-mode-hook        'rainbow-delimiters-mode)
  (add-hook 'c-mode-common-hook   'rainbow-delimiters-mode) ;c++-mode-hook doesnt work?
  (add-hook 'python-mode-hook     'rainbow-delimiters-mode)
  )

(use-package linum
  :config
  ;; (if (eq window-system 'x) (global-linum-mode 1)) ;now set in xframe.el
  (setq column-number-mode t)
  )

(use-package color-theme-approximate
  :config
  ;; (if (not (eq window-system 'x)) (color-theme-approximate-on))
  (color-theme-approximate-on)
  )


;; Pretty colors
;; (add-to-list 'custom-theme-load-path "~/.emacs.d/initfiles/themes/")
;; (load-theme 'deeper-blue t)             ;good for a dark theme 8/10
;; (load-theme 'manoj-dark t)              ;not terrible (dark)- too much contrast 5/10
;; (load-theme 'misterioso t)              ;not too bad (gray blue) bkg is too light 7/10
;; (load-theme 'tango-dark t)              ;pretty good
;; (load-theme 'tsdh-dark t)               ;gray bkgd, not bad 7/10
;; (load-theme 'wombat t)               ;pretty good for gray bkgd 8/10
;; (if (eq window-system 'x)
;;     (load-theme 'db-x t))               ;custom, based off of deeper-blue

;; (if (eq window-system 'nil)
;;     (load-theme 'db-terminal t))        ;custom, based off of deeper-blue
;; (load-theme 'manoj-dark t))              ;ok - dark/high contrast 5/10
;; (load-theme 'misterioso t))              ;so so, too light 6/10
;; (load-theme 'tango-dark t))              ;pretty good 8/10
;; (load-theme 'tsdh-dark t))               ;gray bkgd, nott as good as tango-dark 6/10
;; (load-theme 'wombat t))                  ;pretty good 8/10

;; (add-hook 'window-setup-hook (lambda () (load-theme 'db-x t)))



;; EMACS color-theme %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

;; (use-package color-theme-zenburn)       ;white background, pretty popular
;; (use-package color-theme-sanityinc-tomorrow
;;   :config
;;   (load-theme 'color-theme-sanityinc-tomorrow t))

;; notes:
;; color-theme-solarized/solarized: 6/10, light bg, low contrast
;; zenburn-theme/zenburn: 7/10, grey bg, low contrast
;; color-theme-sanityinc-tomorrow, 8/10 (run color-theme-sanityinc-tomorrow-night)
;; moe-theme/(moe-dark, moe-light):8/10 nice, but really colorful

;; ;; example:
;; (use-package zenburn-theme
;;   :config
;;   (load-theme 'zenburn t)
;; )

;; (setq color-themes '())
(use-package moe-theme
  :config
  (load-theme 'moe-dark t)
  (global-hl-line-mode 1)
  ;; (color-theme-sanityinc-tomorrow-night)
)

;; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




;; EMACS WINDOW OPTIONS

;; git rid of tool bars, splash
(if (functionp 'tool-bar-mode) (tool-bar-mode -1)) ;in case it doesn't exist (cygwin)
(menu-bar-mode -1)
(if (functionp 'scroll-bar-mode) (scroll-bar-mode -1)) ;in case it doesn't exist
(setq inhibit-splash-screen t)

;;start window in upper left corner of screen in X
;; fullscreen function ; no longer works for limestone
;; (defun fullscreen (&optional f)
;;   (interactive)
;;   (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
;;                          '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
;;   (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
;;                          '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0)))
;; (run-with-idle-timer 0.1 nil 'fullscreen)
;; (global-set-key [f8] 'fullscreen)

;; makes actually fullscreen - not just maximized
;; (defun toggle-fullscreen ()
;;   "Toggle full screen on X11"
;;   (interactive)
;;   (when (eq window-system 'x)
;;     (set-frame-parameter
;;      nil 'fullscreen
;;      (when (not (frame-parameter nil 'fullscreen)) 'fullboth))))
;; (run-with-idle-timer 0.1 nil 'toggle-fullscreen)
;; (global-set-key [f8] 'toggle-fullscreen)

;; ;; Gets mostly maximized, but not all the way
;; (custom-set-variables
;;   '(initial-frame-alist (quote ((fullscreen . maximized)))))


;; both of these don't go to  complete full screen
;; (add-to-list 'default-frame-alist '(fullscreen . maximized))
;; (add-to-list 'initial-frame-alist '(fullscreen . maximized))

;; (add-to-list 'default-frame-alist '(fullscreen . maximized))


;; wait until emacs 24.4
;; (toggle-frame-maximized)

;;;
; (add-to-list 'initial-frame-alist '(fullscreen . maximized))

;; this one works pretty well
;; (defun switch-full-screen ()
;;   (interactive)
;;   (with-temp-buffer
;;     (shell-command (concat "wmctrl -i -r " (frame-parameter nil 'outer-window-id)
;;                            " -btoggle,maximized_vert,maximized_horz " ) t)))

;; (if (eq window-system 'x)
;;     (set-frame-position (selected-frame) 0 0)) ;place in upper left of monitors
;; ;; (if (eq window-system 'x)
;; ;;     (run-with-idle-timer 0.1 nil 'switch-full-screen)) ;make fullscreen
;; ;; (if (eq window-system 'x)
;; ;;     (run-with-idle-timer 0.1 nil 'toggle-frame-maximized)) ;make fullscreen
;; (if (eq window-system 'x) (toggle-frame-maximized)) ;make fullscreen


;; emacsclient testing
;; (add-hook 'window-setup-hook 'toggle-frame-maximized t) ;doesn't work
;; (add-hook 'server-visit-hook 'toggle-frame-maximized t) ;doesn't work if i don't pass a filename

;; this is close, but doesn't work if i don't pass a filename
;; (add-hook 'server-visit-hook '(lambda () (set-frame-position (selected-frame) 0 0)) t) ;doesn't work if i don't pass a filename
;; (add-hook 'server-visit-hook 'toggle-frame-maximized t) ;doesn't work if i don't pass a filename

;; (add-hook 'after-make-frame-functions '(lambda () (set-frame-position (selected-frame) 0 0)) t)
;; (add-hook 'after-make-frame-functions 'toggle-frame-maximized)

;; ;; this works
;; (defun make-frame-maximized ()
;;   "modified from frame.el:toggle-frame-maximized
;; Maximize the selected frame or do nothing if it is already maximized.
;; Respect window manager screen decorations.
;; If the frame is in fullscreen mode, don't change its mode,
;; just toggle the temporary frame parameter `maximized',
;; so the frame will go to the right maximization state
;; after disabling fullscreen mode.

;; Note that with some window managers you may have to set
;; `frame-resize-pixelwise' to non-nil in order to make a frame
;; appear truly maximized.

;; See also `toggle-frame-fullscreen'."
;;   (interactive)
;;   (modify-frame-parameters
;;    nil
;;    `((fullscreen . maximized)))
;; )

;; (defun my-frame-behaviours (&optional frame)
;;   "Make frame- and/or terminal-local changes."
;;   (with-selected-frame (or frame (selected-frame))
;;     (set-frame-position (selected-frame) 0 0)
;;     ;; (toggle-frame-maximized)
;;     (make-frame-maximized)
;;     ))
;; (my-frame-behaviours)                   ;for regular emacs
;; (add-hook 'after-make-frame-functions 'my-frame-behaviours) ;for emacsclient


(defun make-frame-maximized (&optional frame)
  "modified from frame.el:toggle-frame-maximized
Maximize the selected frame or do nothing if it is already maximized.
Respect window manager screen decorations.
If the frame is in fullscreen mode, don't change its mode,
just toggle the temporary frame parameter `maximized',
so the frame will go to the right maximization state
after disabling fullscreen mode.

Note that with some window managers you may have to set
`frame-resize-pixelwise' to non-nil in order to make a frame
appear truly maximized.

See also `toggle-frame-fullscreen'."
  (interactive)
  (modify-frame-parameters
   frame
   `((fullscreen . maximized)))
)
