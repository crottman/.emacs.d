;; (add-to-list 'package-archives '("go" . "https://github.com/dominikh/go-mode.el"))

;; elisp
;; (use-package slime
;;   :ensure t)
;; (use-package elisp-format
;;   :ensure t)
;; (use-package agressive-indent
;;   :ensure t)
;; (add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
(add-hook 'emacs-lisp-mode-hook (lambda ()
                                  (local-set-key (kbd "C-j") 'eval-last-sexp)))

;; GO  ::::::::::::::::
(use-package go-mode
  :defer t)
(add-hook
 'go-mode-hook
 (lambda ()
   (define-key go-mode-map (kbd "C-c C-c") 'compile)
   (unless (or (file-exists-p "makefile") (file-exists-p "Makefile"))
     (set (make-local-variable 'compile-command)
          (concat "go build " (file-name-nondirectory buffer-file-name))))
   ))

;; MATLAB
(use-package matlab-mode
  :config
  (autoload 'matlab-mode "matlab" "Enter MATLAB mode." t)
  (setq auto-mode-alist (cons '("\\.m\\'" . matlab-mode) auto-mode-alist))
  (autoload 'matlab-shell "matlab" "Interactcive MATLAB mode." t)
  ;; NOTE: Changes to matlab.el
  ;;    change matlab-indent-level 2 (from 4)
  ;;    change matlab-case-level '(1 . 1) from (2 . 2)
  ;;    can change $$$ % line for better commenting
  (setq matlab-indent-function t); if you want function bodies indented
  (setq matlab-verify-on-save-flag nil) ; turn off auto-verify on save
  (defun my-matlab-mode-hook ()
    (setq fill-column 120)); where auto-fill should auto wrap
  (add-hook 'matlab-mode-hook 'my-matlab-mode-hook)
  (defun my-matlab-shell-mode-hook ()
    '())
  (add-hook 'matlab-shell-mode-hook 'my-matlab-shell-mode-hook)
  )


;; debug what mode is real - why doesn't this always work?
;; (add-to-list 'auto-mode-alist '("\\.tex$" . LaTeX-mode))
;; (add-to-list 'auto-mode-alist '("\\.ltx$" . LaTeX-mode))
;; (add-to-list 'auto-mode-alist '("\\.tex$" . tex-mode))
;; (add-to-list 'auto-mode-alist '("\\.ltx$" . tex-mode))

;; only do this for auctex < 11.89.4
;; (load "auctex.el" nil t t)              ;void function Tex-tex-mode

(add-hook 'LaTeX-mode-hook (lambda ()
 (progn (message "entering LaTeX mode"))
 ))
(add-hook 'latex-mode-hook (lambda ()
 (progn (message "entering latex mode"))
 ))
(add-hook 'tex-mode-hook (lambda ()
 (progn (message "entering tex mode"))
 ))

;; LATEX
;; (add-hook 'LaTeX-mode-hook (lambda ()
;;       (setq TeX-command-default "Rubber")
;;       (define-key LaTeX-mode-map (kbd "C-c c") 'TeX-command-master)
;;       (flyspell-mode 1)                ;spell-check by default
;;       ))
;; (eval-after-load "tex"                  ;add Rubber as a C-c C-c option
;;    '(add-to-list 'TeX-command-list
;; 		'("Rubber" "rubber --pdf '%s.tex' && rubber --clean '%s.tex'  " TeX-run-command nil t) t))

;; Note: LaTeX-mode-hook is for AUCTex, tex-mode-hook or latex-mode-hook is for regular emacs
(add-hook 'latex-mode-hook
          (lambda ()   ;for not using auctex
            (setq-local comment-add 0)
            (define-key latex-mode-map (kbd "C-c C-c") 'compile)
            (define-key latex-mode-map (kbd "C-c C-e") 'latex-insert-block)
            (flyspell-mode 1)                ;spell-check by default
            (unless (or (file-exists-p "makefile") (file-exists-p "Makefile"))
              (set (make-local-variable 'compile-command)
                   (concat "CompileLatex.sh " (file-name-nondirectory buffer-file-name))))))

(add-hook 'LaTeX-mode-hook
          (lambda ()   ;for auctex
            (setq-local comment-add 0)
            (define-key latex-mode-map (kbd "C-c C-c") 'compile) ;doesn't work :(
            (flyspell-mode 1)                ;spell-check by default
            (unless (or (file-exists-p "makefile") (file-exists-p "Makefile"))
              (set (make-local-variable 'compile-command)
                   (concat "CompileLatex.sh " (file-name-nondirectory buffer-file-name))))
            (eval-after-load "tex"                  ;add Rubber as a C-c C-c option
              '(add-to-list 'TeX-command-list
                '("Compile (CompileLatex.sh)" "CompileLatex '%s.tex' "  TeX-run-command nil t) t))))


;; manual auctex loading: works for manually installed emacs
;; (load "auctex.el" nil t t)

;; this gives an error when loading, but maybe is useful for first time loads...
;; (use-package auctex)                    ; breaks (0 calls)
;; fails:     error: Required feature `auctex' was not provided

;; (require 'auctex)                       ;breaks (0 calls)
;; error: Required feature `auctex' was not provided

;; i think this helps auto-install. if not, i may need to manually install
(use-package tex-site
  :ensure auctex
  )


;; R
;; (add-hook 'ess-mode-hook (tool-bar-mode 1)) ;unfortunately, ess is always open
;; (add-hook 'r-mode-hook (tool-bar-mode 1)) ;also doesn't work

;; PYTHON
;; Check python scripts
;; (custom-set-variables
;;  '(py-pychecker-command "CompilePython.sh")
;;  '(py-pychecker-command-args (quote ("")))
;;  '(python-check-command "CompilePython.sh")
;;  )
(add-to-list 'auto-mode-alist '("\\.pyx\\'" . python-mode))
(setq py-pychecker-command-args '(""))
(setq python-check-command "CompilePython.sh")

(add-hook 'python-mode-hook (lambda ()
    (set (make-local-variable 'comment-inline-offset) 2)
    (define-key python-mode-map (kbd "C-c C-c") 'compile)
    ;; Compiling (compiling python == delinting python)
    (unless (or (file-exists-p "makefile") (file-exists-p "Makefile"))
      (set (make-local-variable 'compile-command)
           (concat "CompilePython.sh " (file-name-nondirectory buffer-file-name))))
    (setq electric-indent-chars (delq ?: electric-indent-chars))
    ))


;; Flymake save location - not sure what this does
(defun safer-flymake-find-file-hook ()
  "Don't barf if we can't open this flymake file"
  (let ((flymake-filename
         (flymake-create-temp-inplace (buffer-file-name) "flymake")))
    (if (file-writable-p flymake-filename)
        (flymake-find-file-hook)
      (message
       (format "Couldn't enable flymake; permission denied on %s" flymake-filename)))))
(setq flymake-no-changes-timeout 30)    ;for the most part, only run on save, this half works??
(setq flymake-start-syntax-check-on-newline nil)


;; ;; python flymake with flymake-python
;; (when (load "flymake" t)
;;   (defun flymake-pylint-init ()
;;     (let* ((temp-file (flymake-init-create-temp-buffer-copy
;;                        'flymake-create-temp-intemp)) ;edited from inplace to intemp
;;            (local-file (file-relative-name
;;                         temp-file
;;                         (file-name-directory buffer-file-name))))
;;       (list "~/.emacs.d/initfiles/scripts/pyflymake.py" (list local-file))))
;;       ;; (list "~/git/initFiles/bin/CompilePython.sh" (list local-file))))
;;   ;;     check path
;;   (add-to-list 'flymake-allowed-file-name-masks
;;                '("\\.py\\'" flymake-pylint-init)))

(when (load "flymake" t)
  (defun flymake-pylint-init (&optional trigger-type)
    (let* ((temp-file (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-with-folder-structure))
           (local-file (file-relative-name
                        temp-file
                        (file-name-directory buffer-file-name)))
           (options (when trigger-type (list "--trigger-type" trigger-type))))
      (list "~/.emacs.d/initfiles/scripts/pyflymake.py" (append options (list local-file)))))

  (add-to-list 'flymake-allowed-file-name-masks
                      '("\\.py\\'" flymake-pylint-init)))


(add-hook 'find-file-hook 'safer-flymake-find-file-hook)



;; get rid of the annoying "buffer has a running process" message w/ flymake
(defadvice flymake-start-syntax-check-process
  (after cheeso-advice-flymake-start-syntax-check-1
         (cmd args dir)
         activate compile)
  ;; set flag to allow exit without query on any
  ;;active flymake processes
  (set-process-query-on-exit-flag ad-return-value nil))


;; C/C++  :::::::::
;; C++ compile: Delints as long as there's no makefile
;; (add-hook 'c-mode-common-hook 'google-make-newline-indent)
(use-package google-c-style
  :defer t)
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode)) ;allow header files
(add-hook 'c-mode-common-hook (lambda ()
    (set (make-local-variable 'comment-inline-offset) 2)

    ;; (local-set-key (kbd "C-c C-w") 'compile)
    (local-set-key (kbd "C-c C-c") 'compile)
    (local-set-key (kbd "C-j") 'c-indent-new-comment-line)
    (google-set-c-style)))
;; C++ compile with cppcheck
(add-hook 'c-mode-common-hook (lambda ()
    (unless (or (file-exists-p "makefile") (file-exists-p "Makefile"))
      (set (make-local-variable 'compile-command)
           (concat "~/.emacs.d/bin/cpplint.py " (file-name-nondirectory buffer-file-name))))
    ))


;; CUDA  ::::::::::::::
;; (autoload 'cuda-mode "cuda-mode.el")
(use-package cuda-mode
  :defer t)
(add-to-list 'auto-mode-alist '("\\.cu\\'" . cuda-mode))
(add-hook 'cuda-mode-hook (lambda ()
   (set (make-local-variable 'comment-inline-offset) 2)))
;; cmake :::::::::::::::
;; (autoload 'cmake-mode "cmake" "CMake Mode." t)
;;   (add-to-list 'auto-mode-alist '("\\CMakeLists.txt\\'" . cmake-mode))
(use-package cmake-mode
  :mode "\\CMakeLists.txt\\'")

;; Markdown :::::::::::::::::
(use-package markdown-mode
  :mode "\\.md\\'")
;; (add-to-list 'auto-mode-alist '( . markdown-mode))
