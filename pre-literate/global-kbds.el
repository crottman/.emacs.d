(mapc (apply-partially 'add-to-list 'load-path)
      '("~/.emacs.d/initfiles/"
        "~/.emacs.d/initfiles/packages/"))

;; Comment region or line
(defun comment-or-uncomment-region-or-line ()
  "Comments or uncomments the region or the current line if there's no active region"
  (interactive)
  (let (beg end)
    (if (region-active-p)
        (setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (comment-or-uncomment-region beg end)))

;;Custom keybindings
;; (global-set-key (kbd "<f6>") 'kmacro-start-macro )
;; (global-set-key (kbd "<f7>") 'kmacro-end-macro)
;; (global-set-key (kbd "<f8>") 'kmacro-end-and-call-macro)
;; (global-set-key (kbd "C-;") 'comment-or-uncomment-region-or-line) ;works for GUI only
(global-set-key (kbd "C-_") 'comment-or-uncomment-region-or-line) ;equiv to C-/ in terminal
(global-set-key (kbd "C-/") 'comment-or-uncomment-region-or-line)
(global-set-key (kbd "C-o") 'comment-or-uncomment-region) ;for copy and then comment
(global-set-key (kbd "C-u") 'undo)
;; (global-unset-key "\C-z")               ;stop suspending (annoying)
(global-unset-key (kbd "C-x C-z"))
(global-set-key (kbd "C-z") 'universal-argument)
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)
;;(global-unset-key "\C-xm") ; stop mail, not actually necessary
(global-set-key (kbd "C-x m") 'execute-extended-command) ;instead of mail
(global-set-key (kbd "C-c m") 'execute-extended-command) ;
(global-set-key (kbd "C-c c") 'compile)
(global-set-key (kbd "C-x c") 'save-buffers-kill-emacs) ;for emacsserver
;; (global-set-key (kbd "C-c C-c") 'compile) ;reserve for markdown, etc
(global-set-key (kbd "C-c n") 'next-error)
(global-set-key (kbd "C-u") 'undo)
;; (global-set-key (kbd "C-j") 'c-indent-new-comment-line)
(global-set-key "\C-m" 'newline-and-indent)
(global-set-key (kbd "M-n") 'next-error)
(global-set-key (kbd "<f1>") 'next-buffer)
(global-set-key (kbd "<f2>") 'other-window)

;; directional buffer switching

;; Make Emacs similar to tmux/yakuake
;; (global-set-key (kbd "C-x <up>") 'windmove-up)
;; (global-set-key (kbd "C-x <down>") 'windmove-down)

;; these probably won't be used too often
(global-set-key (kbd "C-x <up>") '(other-window -1))
(global-set-key (kbd "C-x <down>") 'other-window)
(global-set-key (kbd "C-x <left>") 'windmove-left)
(global-set-key (kbd "C-x <right>") 'windmove-right)

;; (global-set-key (kbd "ESC <up>") 'windmove-up) ;like tmux
;; (global-set-key (kbd "ESC <down>") 'windmove-down)
(global-set-key (kbd "ESC o") 'other-window)
(global-set-key (kbd "ESC <down>") 'other-window)
(global-set-key (kbd "ESC <up>") 'previous-multiframe-window)
(global-set-key (kbd "ESC <left>") 'windmove-left)
(global-set-key (kbd "ESC <right>") 'windmove-right)

;; split and move
;; note: these redefine M-letter too :(
(global-set-key (kbd "ESC l") (lambda() (interactive)(split-window-right)(other-window 1)))
;; (global-set-key (kbd "ESC t") (lambda() (interactive)(split-window-right)(other-window 1)))
(global-set-key (kbd "ESC h") (lambda() (interactive)(split-window-below)(other-window 1)))
(global-set-key (kbd "ESC k") 'delete-window)



;; (global-set-key (kbd "<f9>") 'switch-full-screen) ;this is just maximized, not true full screen
(global-unset-key (kbd "<f11>"))
(global-set-key (kbd "<f12>") 'toggle-frame-maximized) ;if i really want it

(global-set-key (kbd "C-x g") 'magit-status)

(global-set-key (kbd "<f5>") 'recompile)
(global-set-key (kbd "<f6>") 'next-error)

;; copy line to ansi-term
(global-set-key (kbd "C-M-<return>") 'copy-line-to-term)
(global-set-key (kbd "C-M-j") 'copy-line-to-term) ;for terminal
