;; (use-package mmm-mode)
;; (setq mmm-global-mode 'maybe)

;; (mmm-add-classes
;;  '(
;;    (embedded-org-emacs-lisp
;;     :submode emacs-lisp
;;     :face mmm-declaration-submode-face
;;     :delimiter-mode nil
;;     :front "#+BEGIN_SRC emacs-lisp[^\n]*"
;;     :back "#+END_SRC")
;;    (embedded-org-ruby
;;     :submode ruby
;;     :face mmm-declaration-submode-face
;;     :delimiter-mode nil
;;     :front "#\\+begin_src ruby"
;;     :back "#\\+end_src")))
;; (mmm-add-mode-ext-class 'org-mode nil 'embedded-org-emacs-lisp)
;; (mmm-add-mode-ext-class 'org-mode nil 'embedded-org-ruby)


;; (use-package mmm-mode
;;   :ensure t
;;   :config
;;   (setq mmm-global-mode 'maybe)
;;   (mmm-add-classes
;;    '((embedded-elisp
;;       :submode emacs-lisp
;;       :face mmm-declaration-submode-face
;;       :front ".*#\\+BEGIN_SRC .*emacs-lisp.*"
;;       :back  ".*#\\+END_SRC.*")))
;;   (mmm-add-mode-ext-class 'org-mode ".*org.*" 'embedded-elisp))

;; (set-frame-parameter (selected-frame) 'alpha '(85 . 50))
;; (add-to-list 'default-frame-alist '(alpha . (85 . 50)))
